/**
 * @description Main config Object
 * @type {{cockpitAuthToken: string, pageTheme: string, cockpitHost: string, debugMode: boolean, pageIsTranslatable: boolean}}
 */
export const applicationSettings = {
    cockpitHost: 'http://127.0.0.1',
    cockpitAuthToken: '',
    debugMode: true,
    pageIsTranslatable: true,
    pageTheme: 'default'
}

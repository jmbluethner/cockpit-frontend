import {applicationSettings} from '../config/app-settings.js';
import * as importer from './importer.js';

export function loadBaseCss() {
    importer.appendCssFile('../themes/'+applicationSettings.pageTheme+'/base/loader.css',function () {})
}
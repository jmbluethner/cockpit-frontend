/**
 * @author jmbluethner <mail@jmbluethner.de>
 */

import {logToConsole} from './logger.js';
import * as urlHandler from './url-handler.js';
import {applicationSettings} from '../config/app-settings.js';

/**
 * @function callApi()
 * @description A general function to talk to the API
 * @param {string} route The URL for the XHR call
 * @param {string} reqBody Request Body
 * @param {string} method GET,POST,DELETE,PUT
 * @param {string} contentType Request content type
 * @param {string} authorization API token
 * @param {function} callback The Callback function gets called when the API responded with Status 200
 * @returns {boolean}
 */
export function callApi(route,reqBody,method,contentType,callback) {
    logToConsole('Trying to call the API at '+applicationSettings.cockpitHost+route,'info');
    let vue = this;
    let xhr = new XMLHttpRequest();
    let url = applicationSettings.cockpitHost + route;
    xhr.onreadystatechange = function() {
        let status = {httpstatus: this.status}
        if(xhr.readyState === 4 && this.status === 200) {
            logToConsole('Received data from the API at '+applicationSettings.cockpitHost+route,'success');
            callback(Object.assign({}, JSON.parse(this.response), status));
        } else if(xhr.readyState === 4 && this.status !== 200) {
            let httpResponseCode = this.status;
            logToConsole('Received data from the API at '+applicationSettings.cockpitHost+route+' but the HTTP response Code was '+httpResponseCode+'. But that might be fine.','warning');
            callback(status);
        }
    };
    xhr.open(method, url, true);
    xhr.setRequestHeader("Content-Type", contentType);
    if(applicationSettings.cockpitAuthToken !== '' && applicationSettings.cockpitAuthToken !== undefined) {
        xhr.setRequestHeader("Cockpit-Token",applicationSettings.cockpitAuthToken);
    }
    xhr.send(reqBody);
}
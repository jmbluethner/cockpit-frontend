/**
 * @author jmbluethner <bluethner@heliophobix.com>;
 * @copyright 2021
 */

import {callApi} from "./api-client.js";
import {getCurrentPage} from "./url-handler.js";
import {logToConsole} from "./logger.js";

/**
 * @description Receive the contents for the current page from the Cockpit API
 * @param {string} pageName Name of the page which has to be requested from cockpit
 * @param {function} callback
 */
export function getPageData(pageName,callback) {
    if(pageName === '' || pageName === '/') {
        pageName = 'index'
    }
    logToConsole('Trying to get page content data from cockpit for the page ' + pageName, 'info');
    callApi('/api/collections/get/pages?filter[pagetitle]=' + pageName, '', 'GET', '', function (response) {
        if (response.httpstatus === 200) {
            callback(response.entries[0]);
        } else {
            let msg = encodeURIComponent('Can\'t get "pages" collection from Cockpit. Got HTTP Status: ' + response.httpstatus);
            window.location.href = "/error?type=http&httpcode="+response.httpstatus;
        }
    });
}
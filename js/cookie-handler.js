import {logToConsole} from "./logger.js";

let cookiesAccepted = false;
let cookiebuffer = [[]];

/**
 * @description Use this function in order to check if the cookie consent was given already
 * @return {boolean}
 */
function checkIfCookiesWhereAccepted() {
    return getCookieByName('cookieconsent') === 'true';
}

/**
 * @description Set the consent cookie
 */
export function acceptCookies() {
    cookiesAccepted = true;
    setCookie('cookieconsent','true','30');
    flushCookieBuffer();
}

export function cookiesConsentGiven() {
    return cookiesAccepted;
}

/**
 * @description Use this function to set new cookies. If the consent was not give, the cookie will be added to the buffer.
 * @param {string} name Cookie name
 * @param {string} value Cookie value
 * @param {string} days Cookie valid time
 */
export function setCookieWithConsent(name,value,days) {
    if(cookiesAccepted) {
        let expires = "";
        if (days) {
            let date = new Date();
            date.setTime(date.getTime() + (days*24*60*60*1000));
            expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + (value || "")  + expires + "; path=/";
    } else {
        logToConsole('New Cookie has to be created but was added to the buffer because the consent is not given yet.','info');
        cookiebuffer.push([name,value,days]);
    }

}

/**
 * @description Use this function to set new cookies which don't require a consent.
 * @param {string} name Cookie name
 * @param {string} value Cookie value
 * @param {string} days Cookie valid time
 */
export function setCookie(name,value,days) {
    let expires = "";
    if (days) {
        let date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}

/**
 * @description This function sets all cookies which have been stored in the buffer
 */
function flushCookieBuffer() {
    logToConsole('Flushing the Cookiebuffer. Contains '+cookiebuffer.length+' Cookies','info')
    cookiebuffer.forEach(function (item,index) {
       setCookie(cookiebuffer[index][0],cookiebuffer[index][1],cookiebuffer[index][2]);
    });
}

/**
 * @description This function searches for a specific cookie and returns the value of it
 * @param {string} name Cookie name to search for
 * @return {string}
 */
export function getCookieByName(name) {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${name}=`);
    if (parts.length === 2) return parts.pop().split(';').shift();
}


document.addEventListener('DOMContentLoaded',function () {
    checkIfCookiesWhereAccepted();
})
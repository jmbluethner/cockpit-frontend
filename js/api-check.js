import {callApi} from "./api-client.js";
import {applicationSettings} from "../config/app-settings.js";

export function checkApiConnection(callback) {
    callApi('/api/singletons/listSingletons','','GET','', function(response) {
        if(response.httpstatus !== 200) {
            let msg = encodeURIComponent('Can\'t get "pages" collection from Cockpit. Got HTTP Status: '+response.httpstatus);
            window.location.href = "/error?type=internal&message="+msg;
        } else {
            callback();
        }
    });
}
export function getBaseUrl() {
    return window.location.origin;
}

export function getCurrentPage() {
    let base_url = window.location.origin;
    let host = window.location.host;
    let pathArray = window.location.pathname.split( '/' );
    return pathArray[pathArray.length-1];
}
export function readGetParameter(param) {
    let result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
            tmp = item.split("=");
            if (tmp[0] === param) result = decodeURIComponent(tmp[1]);
        });
    return result;
}
import * as urlhandler from "./url-handler.js";
import {logToConsole} from "./logger.js";
import {callApi} from "./api-client.js";
import {applicationSettings} from "../config/app-settings.js";

function setPageTitle(title) {
    document.title = title;
}

function setMetaNameTag(name,content) {
    let meta = document.createElement('meta');
    meta.name = name;
    meta.content = content;
    document.getElementsByTagName('head')[0].appendChild(meta);
}

function setMetaPropertyTag(name,content) {
    let meta = document.createElement('meta');
    meta.name = name;
    document.getElementsByTagName('head')[0].appendChild(meta);
    let currTag = document.querySelectorAll('meta[name="'+name+'"]')[0];
    currTag.setAttribute("property",name);
    currTag.setAttribute("content",content);
    currTag.removeAttribute("name");
}

function setPageFavicon(icon) {
    let headTitle = document.querySelector('head');
    let setFavicon = document.createElement('link');
    setFavicon.setAttribute('rel','icon');
    setFavicon.setAttribute('href',icon);
    headTitle.appendChild(setFavicon);
}

export function setPageMeta(pageData) {

    logToConsole('Setting metadata for the page ...','info');

    let globalMetaData;
    callApi('/api/singletons/get/seo','','GET','',function (response) {
        globalMetaData = response;

        // Set page metadata
        if(pageData !== undefined && pageData !== '') {

            // defined per page
            setMetaPropertyTag('og:title',pageData.pagetitle);

            // defined globally
            setPageFavicon(applicationSettings.cockpitHost+''+globalMetaData.favicon.path);
            setMetaNameTag('revisit-after',globalMetaData.revisitafter);
            setMetaNameTag('keywords',globalMetaData.keywords);
            setMetaNameTag('theme-color',globalMetaData.themecolor);
            setMetaNameTag('author',globalMetaData.author);
            setMetaNameTag('viewport',globalMetaData.viewport);
            setMetaPropertyTag('og:image',applicationSettings.cockpitHost+''+globalMetaData.ogimage.path);

            // combined
            let devider = '';
            if(globalMetaData.websitename !== '' && globalMetaData.websitename !== null && pageData.pagetitle !== '' && pageData.pagetitle !== null) {
                devider = ' - ';
            }
            setPageTitle(globalMetaData.websitename+''+devider+''+pageData.pagetitle);
            if(pageData.pagedescription === '') {
                setMetaNameTag('description',globalMetaData.pagedescription);
                setMetaPropertyTag('og:description',globalMetaData.pagedescription);
            } else {
                setMetaNameTag('description',pageData.pagedescription);
                setMetaPropertyTag('og:description',pageData.pagedescription);
            }

        } else {

        }

    });

}
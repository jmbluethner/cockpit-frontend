import {callApi} from "./api-client.js";
import {logToConsole} from './logger.js';

export function sendAnonymousStatistics() {
    let body = {
        appCodeName: navigator.appCodeName,
        appName: navigator.appName,
        appVersion: navigator.appVersion,
        language: navigator.language,
        platform: navigator.platform,
    }
    body = JSON.stringify(body);
    callApi('/api/stats/sendClientData',body,'POST','application/json',function (response) {
        if(response.status === 200) {
            logToConsole('Successfully sent anonymous data to the host.','success');
        } else {
            logToConsole('Internal Error while trying to send userdata. Got Code '+response.status,'error');
        }
    });
}
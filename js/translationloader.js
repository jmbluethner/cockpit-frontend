import {translations} from '../translations/translations.js';

function collectAllTranslatableObjects() {
    return document.querySelectorAll('[data-translation-identifier]');
}

export function translate(language) {
    collectAllTranslatableObjects().forEach(function (item,index) {
        let itemTranslationIdentifier = item.dataset.translationIdentifier
        item.innerHTML = translations[itemTranslationIdentifier][language];
    })
}
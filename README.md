# Cockpit Frontend - COF

A VueJS frontend which connects to a <a href="https://getcockpit.com/" target="_blank">Cockpit CMS</a> instance.   
<br>
<i>By jmbluethner</i><br><br>

---

<img width="100%" src="https://gitlab.com/jmbluethner/cockpit-frontend/-/wikis/uploads/6a5565d653e8104e6d6a7db00f27241f/doku_features.svg" /><br/>

<br>

![Badge](https://gitlab.com/jmbluethner/cockpit-frontend/badges/main/pipeline.svg)
![Badge](https://img.shields.io/gitlab/v/tag/jmbluethner/cockpit-frontend)
![Badge](https://img.shields.io/badge/Verified-Cool%20repo%20%F0%9F%95%B6-orange)
![Badge](https://img.shields.io/badge/Tested%20Cockpit%20Version-v0.12.2-blue)
![Badge](https://img.shields.io/badge/Tested%20Browsers-Chrome%2C%20Firefox-green)
![Badge](https://gitlab.com/jmbluethner/cockpit-frontend/badges/main/coverage.svg)
![Badge](https://img.shields.io/badge/License-MIT-green)

<br>

---

> <b style="color: rgb(255,150,0);">⚠ Disclaimer ⚠</b><br><b>This Repo is work in progress and currently just in its alpha phase. You are more than welcome to check it out and play with it, but you will run into problems such as missing mobile support.<br>
> If you want to use COF in a (hopefully) stable state, please come back later.

---

<br>

## ❤ Before we start  

For me, COF is a little fun project that might have the potential to help some people when building pages with Cockpit. I think it can really speed up your workflow when it comes to building pages with Cockpit.  
Therefore, I really don't want anyone to get frustrated when trying to set up COF and that's why I urge you to reach out to me if there are any problems with COF. I might not respond fast, but I'll try my best to help you out in any situation in which you might get stuck.  
So if there is anything I can do for you, feel free to E-Mail me to mail@jmbluethner.de  

Also, please be aware of the fact that I'm not a professional developer. I'm trying my best to code as clean as possible, but I'm sure my code is messy in some places. So don't use this repo as a reference for clean or perfect code 😄    

Now - Have fun with COF!

<hr>

[[_TOC_]]

<hr>

## 📚 Features

- Frontend built on VueJS -> Dynamic, Adaptable, Fast
- In-Depth Browser console logging for easy problem-solving
- Centralized config file
- Requires just a <code>.htaccess</code> supporting Webserver. No PHP, Node and so on.
- I <b>tried</b> to code as clean as possible, so you can reverse-engineer my project.
- Nested components, like Sections and Grids, work without a problem. Nest however deep you want!

## ❗ Remarks

Please be aware of the following things before starting to use COF:
- This Repo is a private project primarily for learning how Cockpit CMS works. Bugs will be fixed, but it might take some time.
- I'm neither a JS nor a Cockpit professional. Don't see this repo as a 'perfect way' to approach a cockpit based frontend. Feel free to tell me if there are smarter ways to do things!

## ⚙️ Frontend Setup / Getting started
### Requirements

Good new: We don't need that much.  
COF needs to run on a webserver like Apache, so the .htaccess rewrite rules can affect the filenames as intended.  
And that's it!

> <b style="color: rgb(0,160,255);">Hint</b><br>COF <b>is not</b> meant to be running on a subdirectory of your webhost! <br>Make sure to have two seperate hosts in palce, one for the Backend (Cockpit) and one for the frontend (COF).<br>I recommend creating an admin subdomain for that purpose. This could thank look like ...<br>- mydomain.com -> COF<br>- admin.mydomain.com -> Cockpit

### Installation

Grab the <a href="https://gitlab.com/jmbluethner/cockpit-frontend/-/tags" target="_blank">latest release</a> of COF and simply put all the files in your webservers root directory. Simple, isn't it?  
If you haven't installed Cockpit yet, <a href="https://github.com/agentejo/cockpit/releases/tag/0.12.2" target="_blank">grab the latest (verified working) version</a>.

Optionally COF ships with a pre-configured base structure for Cockpit. This allows you to get going right away without having to manually create and configure the singletons and collections as well as their fields.  

> <b style="color: rgb(0,160,255);">Hint</b><br>Unfortunately I can't find a way to also export the custom components. Therefore, you'll have to create the custom components by hand. See more [here](#supported_custom_components). 

If you want to use this as a starting point, you'll have to import the collections and singletons found in <code>/getting-started/singletons</code> and <code>/getting-started/collections</code>.  
If you don't know how to do imports in Cockpit, check out the [official CLI documentation](https://getcockpit.com/documentation/reference/CLI).  
In case you need help with this, feel free to hit me up 🖖

### Basic configuration

Now you have to make some configurations in order to run COF. The good thing is, that all the required configuration variables are stored in one centralized file. So you don't have to jump between various files.  
The main config file can be found under <code>/config/app-settings.js</code> - Straight forward.  
Once you open the file, you will see some variables.

> <b style="color: rgb(0,160,255);">Hint</b><br>The config file might be different between versions. Please keep that in mind if the version you are using isn't identical with the version this Guide was made for.

> <b style="color: rgb(255,150,0);">Warning</b><br>Keep in mind that we are configuring inside of a JS file. That means that this file gets loaded by <b>the client</b>. Therefore, don't use an admin auth token! Either create an own user group in Cockpit or just make the data public. People can watch your page to see the content anyways.

The config file looks something like this:

```js
export const applicationSettings = {
    cockpitHost: 'http://127.0.0.1',
    cockpitAuthToken: '',
    debugMode: true,
    pageIsTranslatable: true,
    pageTheme: 'default'
}
```

> <b style="color: rgb(0,160,255);">Hint</b><br>Make sure to include the protocol, <code>http</code> in this case, in the <code>cockpitHost</code> config variable.


| Key                | Usage                                                                                                                                                                                                                         | 
|--------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| cockpitHost        | The IP or URL of your Cockpit instance.                                                                                                                                                                                       |
| cockpitAuthToken   | The Token which COF has to use when connecting to the Cockpit API. Leave this empty if you've configured your collections and singletons to be public (Recommended. Storing API keys on the client side is insecure anyways.) |
| debugMode          | When set to true, COF will show detailed logging information in the browsers console.                                                                                                                                         |
| pageIsTranslatable | Enables / Disables the language switcher.                                                                                                                                                                                     |
| pageTheme          | Can manipulate the path for all component specific CSS files.                                                                                                                                                                 |

> <b style="color: rgb(0,160,255);">Hint</b><br>If you see an error page when using COF for the first time, make sure to check if you've defined the <code>cockpitHost</code> within the config file and/or if the specified host is correct.

## ✈️ Cockpit base configuration

In order for COF to work properly, there are some collections and singletons which have to be created inside your Cockpit CMS instance. Those are:

| Type       | Name   | Role                                                                               | Required Fields                                                                                                                                 |
|------------|--------|------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------|
| Singleton  | seo    | Basic page information for tags inside of the html head                            | revisitafter: string<br>keywords: string<br>themecolor: string/hex<br>favicon: image<br>websitename: string<br>author: string<br>ogimage: image |
| Singleton  | navbar | Menu points and Logo for the navigation bar                                        | Read more [here](#s_navbar)                                                                                                                     |
| Singleton  | footer | Define links, legal notice information, social media channels and copyright holder | Read more [here](#s_footer)                                                                                                                     |
| Collection | pages  | All of your web pages                                                              | pagetitle: string<br>pagedescription: string<br>pagecontent: layout                                                                             |

<img src="https://gitlab.com/jmbluethner/cockpit-frontend/-/wikis/uploads/2b18b43b66eb39b612758a9785a04f91/image.png" />

<h3 id="s_navbar">Navbar</h3>

Required fields for defining the Navbar  

<table>
    <tr>
        <th>Field Name</th>
        <th>Type</th>
        <th>Function</th>
        <th>Example (if needed)</th>
    </tr>
    <tr>
        <td>phone</td>
        <td>text</td>
        <td>Show your phone number</td>
        <td></td>
    </tr>
    <tr>
        <td>email</td>
        <td>text</td>
        <td>Show your E-Mail Address</td>
        <td></td>
    </tr>
    <tr>
        <td>socials</td>
        <td>Object</td>
        <td>Show Social Media Icons that link to your channels</td>
        <td>
<pre>
[
    {
        "link": "https://instagram.com",
        "icon": "fab fa-instagram"
    },
    {
        "link": "https://facebook.com",
        "icon": "fab fa-facebook"
    }
]
</pre>
        Note: The icon names can be found <a href="https://fontawesome.com/search" target="_blank">here</a>
        </td>
    </tr>
    <tr>
        <td>logo</td>
        <td>File</td>
        <td>Logo picture which gets shown on the left side of the navbar</td>
        <td></td>
    </tr>
    <tr>
        <td>entries</td>
        <td>Object</td>
        <td>This Object field defines the actual navigation links in the navbar.<br>In order to create a big mega menu you can define submenus by adding an Array called "submenus" into your Array of navbar entry points. Inside of the "submenus" Array you can than define groups of navigation links by creating another array with the name that your group should receive. In the example on the right they are called "Starter Line" and "Pro Line". Inside of those nested Arrays you can than create your navigation links as you would outside of the nested structure.<br>Hint: The "link" attribute from the nested parent, titled "Products" in the example, gets added to all links that follow in the nested objects. Referring to the example on the right, the links inside of the "Starter Line" would be <code>/products/starter-line/radios</code> and <code>/products/starter-line/beacons</code>. </td>
        <td>
<pre>
[
	{
		"link": "/about",
		"target": "",
		"title": "About"
	},
	{
		"link": "/contact",
		"target": "",
		"title": "Contact"
	},
	{
		"link": "/products",
		"target": "",
		"title": "Products",
		"submenus": [
			{
				"name": "Starter Line",
				"links": [
					{
					  "link": "/starter-line/radios",
					  "target": "",
					  "title": "Radios"
					},
					{
					  "link": "/starter-line/beacons",
					  "target": "",
					  "title": "Beacons"
					}
				]
			},
			{
				"name": "Pro Line",
				"links": [
					{
					  "link": "/pro-line/radios",
					  "target": "",
					  "title": "Radios"
					},
					{
					  "link": "/pro-line/beacons",
					  "target": "",
					  "title": "Beacons"
					}
				]
			}
		]
	}
]
</pre>
        </td>
    </tr>
</table>

<h3 id="s_footer">Footer</h3>

Required fields for defining the Footer

<table>
    <tr>
        <th>Field Name</th>
        <th>Type</th>
        <th>Function</th>
        <th>Example (if needed)</th>
    </tr>
    <tr>
        <td>
            Column_1
        </td>
        <td>
            Wysiwyg
        </td>
        <td rowspan="4">Text and/or links to show in the first column. Make sure to format the headlines as bold text.</td>
        <td rowspan="4">
            <b>About Us</b>
            <ul>
                <li>History</li>
                <li>Team</li>
                <li>Sales locations</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>
            Column_2
        </td>
        <td>
            Wysiwyg
        </td>
    </tr>
    <tr>
        <td>
            Column_3
        </td>
        <td>
            Wysiwyg
        </td>
    </tr>
    <tr>
        <td>
            Column_4
        </td>
        <td>
            Wysiwyg
        </td>
    </tr>
    <tr>
        <td>legal_notice</td>
        <td>Object</td>
        <td>Legal notice links</td>
        <td>
<pre>
[
    {
        "legal": "Imprint",
        "link": "/imprint"
    },
    {
        "legal": "TOS",
        "link": "/tos"
    }
]
</pre>
        </td>
    </tr>
    <tr>
        <td>social_links</td>
        <td>Object</td>
        <td>Show icons which link to your socials</td>
        <td>
<pre>
[
    {
        "link": "https://instagram.com",
        "icon": "fab fa-instagram"
    },
    {
        "link": "https://facebook.com",
        "icon": "fab fa-facebook"
    }
]
</pre>
        </td>
    </tr>
</table>

<h2 id="supported_custom_components">📦 Supported Custom Components</h2>

COF supports all components which Cockpit ships with by default.  
But wait, there's more! COF also supports some custom components.  

> <b style="color: rgb(0,160,255);">Hint</b><br>In order to create those custom components in Cockpit, it's necessary to install the <a href="https://github.com/agentejo/LayoutComponents" target="_blank">LayoutComponents</a> extension from agentejo.

Here you can see all the custom components as well as how you have to configure them inside of Cockpit:

| Component          | Features                                                                                                                                                                                                                                                                                     | Fields                                                                                                                                                                                                                                                                                                                                                                                                                                      | Implementation Status                                                       |
|--------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------|
| map                | Allows you to display a Map with an unlimited amount of markers, custom centerpoint and a custom default zoomlevel                                                                                                                                                                           | coordinates: JSON data for the markers<br/>centerpoint: coordinates of a point on the map which has to be centered upon loading the map in the frontend<br/>zoomlevel: A number from 1-12 to define how much the map has to be zoomed in by default<br/>markericon: Image for the markers which you have defined earlier in the coordinates field.<br/>See more [here](#maps)                                                               | <img src="https://img.shields.io/badge/Status-Implemented-green">           |
| form               | The corresponding component for the Forms function that ships with Cockpit.                                                                                                                                                                                                                  | fields: JSON Data<br/>targetform: The name of the target form in Cockpit.<br/>See details [here](#forms)                                                                                                                                                                                                                                                                                                                                    | <img src="https://img.shields.io/badge/Status-Implemented-green">           |
| youtubevideo       | Allows you to embed a YouTube Video on your page.                                                                                                                                                                                                                                            | url: Text field for the video URL.                                                                                                                                                                                                                                                                                                                                                                                                          | <img src="https://img.shields.io/badge/Status-Implemented-green">           |
| spacer             | Just a block with a custom height to make some horizontal space between elements on your page.                                                                                                                                                                                               | height: Text field for the height. Make sure to include the unit, like '10px' or '5mm'                                                                                                                                                                                                                                                                                                                                                      | <img src="https://img.shields.io/badge/Status-Implemented-green">           |
| video              | Allows you to play a video file on your page.                                                                                                                                                                                                                                                | file: File upload for the video                                                                                                                                                                                                                                                                                                                                                                                                             | <img src="https://img.shields.io/badge/Status-Implemented-green">           |
| 3dmodel            | Sick, right? You can show a 3D Model on your website with this component. The model is completely interactive on your page, so the user can rotate, move and zoom in on it. But be careful when it comes to the file size of your 3D model. Remember that the client has to download that ;) |                                                                                                                                                                                                                                                                                                                                                                                                                                             | <img src="https://img.shields.io/badge/Status-Disabled%20%2F%20Bugged-red"> |
| banner             | A big picture with 100% width. It can also show text as well as a button for call to action purposes.                                                                                                                                                                                        | picture: The photo you want to use<br/>text: Text for the banner<br/>buttonText: Text on the CTA Button<br/>buttonLink: Link for the CTA button<br/>buttonTarget: Target attribute for the button (for example target="_blank")                                                                                                                                                                                                             | <img src="https://img.shields.io/badge/Status-Implemented-green">           |
| tile               | Works a bit like a banner, but it's meant to being used in a column.                                                                                                                                                                                                                         | picture: The photo you want to use<br/>text: Text for the banner<br/>buttonText: Text on the CTA Button<br/>buttonLink: Link for the CTA button<br/>buttonTarget: Target attribute for the button (for example target="_blank")<br/>overlayColor: Use this text field to define an overlay color for the background image. It's recommended to use an alpha value of about 0.8 on the color. Leave this blank if you don't want an overlay. | <img src="https://img.shields.io/badge/Status-Implemented-green">           |
| timelineVertical   | This component allows you to show events in the form of a vertical timeline.                                                                                                                                                                                                                 | This component gets configured by a JSON field. See more [here](#timelines)                                                                                                                                                                                                                                                                                                                                                                 | <img src="https://img.shields.io/badge/Status-To%20be%20done-yellow">       |
| timelineHorizontal | This component allows you to show events in the form of a horizontal timeline.                                                                                                                                                                                                               | This component gets configured by a JSON field. See more [here](#timelines)                                                                                                                                                                                                                                                                                                                                                                 | <img src="https://img.shields.io/badge/Status-To%20be%20done-yellow">       |


<i>More coming soon ... :) </i>

<h3 id="maps">The map component</h3>
Because the maps component has the most tricky setup out of all the components listed above, I'll give you a short explenation on how to configure it more in depth here.  
As already mentioned, the map component can be configured using four fields.  
- coordinates
- centerpoint
- zoomlevel
- markericon

The zoomlevel and the markericon are pretty easy to set up. Take a look at the table above if you need more infos and haven't read that yet.  
The coordinates as well as the centerpoint are defined through JSON data. Examples might look like this:

#### Coordinates

```json
[
  [
    103.8472445,
    1.2900143,
    "Marker Name"
  ],
  [
    -80.2854359,
    25.9759462,
    "Marker Name"
  ],
  [
    8.0934123,
    48.7808663,
    "Marker Name"
  ]
]
```

#### Centerpoint

```json
[
  8.0934123,
  30.7808663
]
```

Now that you saw the examples... It's pretty easy isn't it?


<h3 id="timelines">Timelines</h3>

Both the vertical and the horizontal timeline components require a JSON field to define their contents.

```json
[
  {
    "dat": "Date and Location information",
    "text": "Entry details ...",
    "linkText": "Read more",
    "linkContent": "https://example.com",
    "linkTarget": "_blank"
  },
  {
    "dat": "Date and Location information",
    "text": "Entry details ...",
    "linkText": "Read more",
    "linkContent": "https://example.com",
    "linkTarget": "_blank"
  }
]
```


## 🖼️ Style it

Individually styling all the components and cockpit partials is actually pretty easy.  
Every partial, and therefore also every component, loads its own CSS. That allows you to change the CSS for all the components and cockpit partials without having to dig through huge CSS files.  
You can find the responsible CSS files under <code>/themes/{your_theme_here OR default}/components/</code> and <code>/themes/{your_theme_here OR default}/cockpit-partials/</code>     
When looking for these files, you might see the <code>/themes/{your_theme_here OR default}/base/</code> folder. This is responsible for all the global definitions like color variables, fonts, the grid layout and so on.  
If you want to just change the colors from the default theme, head over to the <code>/themes/{your_theme_here OR default}/base/</code> folder and change the definitions in the <code>colors.css</code> file.  

If you decide to create your own Theme in <code>/themes/{your_theme_here}/</code> you have to tell COF to use your theme instead of the default theme. This can be easily achieved by changing the <code>pageTheme</code> attribute in the <code>/config/app-config.js</code> file.

<h2 id="forms"> ✉️ Forms</h2>

Using forms is very... Easy in fact!  
To use a form in your frontend, follow these steps:

1. Create the form in Cockpit
2. Import the form partial from <code>/views/cockpit-partials/form.js</code> into your page JS, define it as a component and use it in your template.  
3. Now you need to tell the form component two things.
   1. Which form in Cockpit should be targeted, or which form should receive the data, if the user submits data from the form  
   2. What fields should the form have

> <b style="color: rgb(0,160,255);">Hint</b><br>Make sure to <b>not</b> name your component "form" since this is a reserved DOM object. Give it a name along the lines of "customform" or something like that.

In order to tell your form component the required things mentioned above in step 3, use a <code>v-bind:data</code> within your component in the template. That might look like this:

```js
<customform v-bind:data='{"targetForm":"myAwesomeCockpitForm","fields":[{"tag": "input", "type":"text","style":"width:100%;display:block;background:red;","placeholder":"Myfield","required":true},{"tag": "input", "type":"text","style":"width:100%;display:block;background:blue;","placeholder":"Mysecondfield","required":true}]}'></customform>
```

## 🔬 How it works (da coooode)
### When the user requests a page

> <b style="color: rgb(0,160,255);">Hint</b><br>This section doesn't explain every single step in the code. If I'd do that, this file would be very very large. But I've tried my best to code clean, so you can look into it to understand what I've done and how everything works.

The user lands on the according html file. For example <code>index.html</code>. The html file just contains minimal code. It sets the basic structure, imports it's Vue page and that's it.

```html
<!DOCTYPE html>
<html lang="de">
    <head>
        <meta charset="UTF-8">
    </head>
    <body>
        <div id="app"></div>
    </body>
    <script type="module" src="views/pages/home.js"></script>
</html>
```

Also take a look at the only <code>div</code> in the <code>body</code>. This element is essential for the page to work, because this is the container for all following Vue processes. Therefore, this one <code>div</code> <b>always</b> has to have the ID <code>app</code>.  

The next thing that happens is, that the requested JS file in <code>views/pages/</code> collects all the components which are required for the page. It than builds the DOM with those components.  

```js
/** 
 * A lot of imports ...
 * Take a look into the actual file to inspect closer.
 */

// Import components
import {navbar} from "../components/navbar.js";
import {pagecontents} from "../components/pagecontent.js";

// Main Component
prepareVue(function () {

    // Load the basic CSS structure
    loadBaseCss();

    logToConsole('Initializing Vue views...','info');


    let bus = {};
    bus.eventBus = new Vue();

    // Create the app
    let app = new Vue({
        el: '#app',
        // Define the required components
        components: {
            navbar: navbar,
            pagecontent: pagecontents
        },
        // Create a variable for the page data which we'll get from cockpit
        data: function () {
            return {
                pageData: {}
            }
        },
        // Execute this once the page is built
        mounted: function() {
            let vue = this;

            // Get the name of the current page by reading the URL and removing the path.
            // See getCurrentPage() in /js/url-handler.js
            let currPage = getCurrentPage();
            cockpit.getPageData(currPage,function (response) {
                vue.pageData = response;
                // Define the page meta attributes in the DOM header, such as the title and description
                setPageMeta(vue.pageData);
                // Tell the pagecontent (ref="contentcontainer") sub-component to fetch the just received API data from this root component 
                vue.$refs.contentcontainer.fetchData();
            });

        },
        // Define the template and use the components defined earlier
        template:
            `
            <div>
                <navbar v-bind:backlayNavbar="true" />
                <pagecontent ref="contentcontainer"></pagecontent>
            </div>
            `
    });
});
```

Everything that's happening above should be straight forward to anyone familiar with VueJS.  
The next thing which happens and which is, at least in my opinion, the most interesting, is the <code>pagecontent</code> component.  
It is responsible for showing all the content which was configured by the user in the <code>pages</code> Cockpit collection.  

> <b>Developer's annotation</b>  
> <i>"This was by far the most pain in the ass in the scope of the entire project."</i>

The <code>pagecontent</code> component is located under <code>/views/components/pagecontent.js</code>

```js
// Import Settings
import {applicationSettings} from "../../config/app-settings.js";

// Import Cockpit partials (CMS objects for the layout field type)
import {cockpit_headings} from "../cockpit-partials/heading.js";
// ...

const pagecontents = {
    data: function () {
        return {
            data: {},
            applicationSettings: applicationSettings
        }
    },
    methods: {
        // Get data from the root component. Function gets triggered by root.
        fetchData: function() {
            this.data = this.$root.pageData.pagecontent;
            this.normalizeData();
        },
        // Go through the received data and normalize/default some things
        normalizeData: function () {
            let vue = this;
            this.data.forEach(function (item,index) {
                switch(item.component) {
                    case 'heading':
                        if(item.settings.tag === null) {
                            item.settings.tag = 'h1';
                        }
                        break;
                    case 'grid':

                        break;
                }
            });
        }
    },
    mounted: function () {
        let vue = this;
    },
    components: {
        c_heading: cockpit_headings,
        c_image: cockpit_images,
        c_grid: cockpit_grid,
        c_section: cockpit_section
    },
    template:
        `
        <section class="cockpit-container">
            <div v-for="content in data" :data-cockpit-component="content.component">
            
                <c_heading v-if="content.component === 'heading'" v-bind:data="content"></c_heading>
                <c_image v-if="content.component === 'image'" v-bind:data="content"></c_image>      
                <c_grid v-if="content.component === 'grid'" v-bind:data="content"></c_grid>
                <c_section v-if="content.component === 'section'" v-bind:data="content"></c_section>
                <!-- ... -->
            </div>
        </section>
        `
}

// Export the component
export {pagecontents}
```

## 🌠 Sources / Honorable mentions  
- https://discourse.getcockpit.com/ <b>Special thanks to <a href="https://23go.de">@abernh</a> !</b>
- https://getcockpit.com/documentation
- https://github.com/agentejo/LayoutComponents
- https://github.com/serjoscha87/cockpit_GROUPS
- https://vuejs.org/

And just because I really love it ...
- https://www.jetbrains.com/de-de/webstorm/

And because those guys inspired me to teach myself how to code without even knowing it ...
- https://github.com/Grenzdebil
- https://github.com/DevonCrawford

<br>
<br>

---
<i>
Developed with ♥, passion, a lot (A. Lot.) of ☕ and VueJS by jmbluethner<br>
Made & Maintained in 2021-2022
</i><br><br>
<img height="50px" src="https://gitlab.com/jmbluethner/cockpit-frontend/-/wikis/uploads/91ad7656cfe34d4a98eb984442790877/sign.svg" /><br>_

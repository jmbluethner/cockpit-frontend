import * as importer from '../../js/importer.js';
import {applicationSettings} from "../../config/app-settings.js";

const cockpit_map = {
    props: {
        data: Object
    },
    data: function () {
        return {

        }
    },
    mounted: function() {
        let vue = this;

        let markers = [];
        vue.data.settings.coordinates.forEach(function (coord,index) {
            markers.push(coord);
        });

        importer.appendJsFile('/lib/openlayers/ol.js', 'text/javascript', function (response) {

            const map = new ol.Map({
                target: 'map',
                layers: [
                    new ol.layer.Tile({
                        source: new ol.source.OSM()
                    })
                ],
                view: new ol.View({
                    center: ol.proj.fromLonLat(vue.data.settings.centerpoint),
                    zoom: vue.data.settings.zoomlevel
                })
            });

            markers.forEach(function (item,index) {

                let marker = new ol.Feature({
                    geometry: new ol.geom.Point(
                        ol.proj.fromLonLat([item[0],item[1]])
                    ),  // Cordinates of New York's City Hall
                });

                marker.setStyle(new ol.style.Style({
                    image: new ol.style.Icon(({
                        crossOrigin: 'anonymous',
                        src: applicationSettings.cockpitHost+'/'+vue.data.settings.markericon.path,
                        // src: '/media/img/becker_logo.png',
                        scale: .5
                    }))
                }));

                let vectorSource = new ol.source.Vector({
                    features: [marker]
                });
                let markerVectorLayer = new ol.layer.Vector({
                    source: vectorSource,
                });
                map.addLayer(markerVectorLayer);

            });

        });
    },
    methods: {

    },
    template:
        `
        <div class="map_container">
           <div id="map"></div>
        </div>
    `
}

// Load the components CSS
importer.appendCssFile('/themes/'+applicationSettings.pageTheme+'/cockpit-partials/map.css',function () {});

// Export the component
export {cockpit_map}
import * as importer from '../../js/importer.js';
import {applicationSettings} from "../../config/app-settings.js";

const cockpit_spacer = {
    props: {
        data: Object
    },
    data: function () {
        return {

        }
    },
    mounted: function() {
    },
    methods: {

    },
    template:
        `
        <div class="cockpit_spacer" :style="'height: '+data.settings.height"></div>
    `
}

// Load the components CSS
importer.appendCssFile('/themes/'+applicationSettings.pageTheme+'/cockpit-partials/spacer.css',function () {})

// Export the component
export {cockpit_spacer}
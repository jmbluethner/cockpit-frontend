import {applicationSettings} from "../../config/app-settings.js";
import * as importer from "../../js/importer.js";

const cockpit_gallery = {
    props: {
        data: Object
    },
    data: function () {
        return {
            applicationSettings: applicationSettings,
            showLightbox: false,
            lightboxImage: ''
        }
    },
    methods: {
        showImageInLightbox(imageId) {
            let vue = this;
            let image = document.getElementById(imageId);
            let lightbox = document.getElementById('lightbox');
            vue.showLightbox = !vue.showLightbox;
            vue.lightboxImage = image.src;
        },
        closeLightbox() {
            this.showLightbox = false;
        }
    },
    mounted: function () {
        let vue = this;
        document.body.addEventListener('keypress', function(e) {
            if (e.key === "Escape") {
                vue.closeLightbox();
            }
        });
    },
    template:
        `
        <div class="gallery_container">
            <div class="image_container" v-for="(img,index) in data.settings.gallery">
                <img :id="'img_'+index" loading="lazy" :src="applicationSettings.cockpitHost+''+img.path"/>
                <div class="overlay" @click="showImageInLightbox('img_'+index)">
                    <div class="inner">
                        <div class="container">
                            
                        </div>
                    </div>
                </div>
            </div>
            <div v-if="showLightbox" id="lightbox" @click="closeLightbox()">
                <div id="inner_image" :style="{'background-image': 'url(' +lightboxImage+ ')'}"></div>
                <div class="controls">
                    <div class="left">
                        
                    </div>
                    <div class="right">
                        <i @click="closeLightbox()" class="far fa-times-circle"></i>
                    </div>
                </div>
            </div>
        </div>
        `
}

importer.appendCssFile('/themes/'+applicationSettings.pageTheme+'/cockpit-partials/gallery.css',function () {})

export {cockpit_gallery}
import * as importer from "../../js/importer.js";
import {applicationSettings} from "../../config/app-settings.js";

const cockpit_video = {
    props: {
        data: Object,
    },
    data: function() {
        return {
            applicationSettings: Object
        }
    },
    mounted: function () {
        let vue = this;
        vue.applicationSettings = applicationSettings;
    },
    template:
        `
        <div class="video_container fullwidth">
            <video autoplay loop muted>
                <source :src="applicationSettings.cockpitHost+'/'+data.settings.file">
            </video>
            <div class="overlay">
                <h2>Adaptive. Responsive. Reliable.</h2>
            </div>
        </div>
        `
}

importer.appendCssFile('/themes/'+applicationSettings.pageTheme+'/cockpit-partials/video.css',function () {})

export {cockpit_video}
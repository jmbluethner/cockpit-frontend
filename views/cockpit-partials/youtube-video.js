import * as importer from '../../js/importer.js';
import {applicationSettings} from "../../config/app-settings.js";

const cockpit_youtube_video = {
    props: {
        data: Object
    },
    data: function () {
        return {

        }
    },
    mounted: function() {

    },
    methods: {

    },
    template:
        `
            <div class="cockpit_youtube_video_container" :style="data.settings.style">
                <iframe :src="data.settings.URL" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        `
}

// Load the components CSS
importer.appendCssFile('/themes/'+applicationSettings.pageTheme+'/cockpit-partials/youtube-video.css',function () {})

// Export the component
export {cockpit_youtube_video}
import * as importer from "../../js/importer.js";
import {applicationSettings} from "../../config/app-settings.js";

const cockpit_headings = {
    props: {
        data: Object
    },
    template:
        `
        <div class="heading_container">
            <h1 v-if="data.settings.tag === 'h1' || data.settings.tag === null">{{data.settings.text}}</h1>
            <h2 v-if="data.settings.tag === 'h2'">{{data.settings.text}}</h2>
            <h3 v-if="data.settings.tag === 'h3'">{{data.settings.text}}</h3>
            <h4 v-if="data.settings.tag === 'h4'">{{data.settings.text}}</h4>
            <h5 v-if="data.settings.tag === 'h5'">{{data.settings.text}}</h5>
            <h6 v-if="data.settings.tag === 'h6'">{{data.settings.text}}</h6>
        </div>
        `
}

importer.appendCssFile('/themes/'+applicationSettings.pageTheme+'/cockpit-partials/heading.css',function () {})

export {cockpit_headings}
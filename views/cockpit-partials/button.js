import * as importer from "../../js/importer.js";
import {applicationSettings} from "../../config/app-settings.js";

const cockpit_button = {
    props: {
        data: Object
    },
    mounted: function () {
        let vue = this;
        // Detect if the address is local. If so, automatically add the current origin. http://127.0.0.1 for example.
        if(vue.data.settings.url.charAt(0) === '/') {
            vue.data.settings.url = window.location.origin+''+vue.data.settings.url;
        }
    },
    template:
        `
        <a :href="data.settings.url">
            <button class="primary cockpit">{{data.settings.text}}</button>
        </a>
        `
}

importer.appendCssFile('/themes/'+applicationSettings.pageTheme+'/cockpit-partials/button.css',function () {})

export {cockpit_button}
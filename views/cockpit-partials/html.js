import * as importer from "../../js/importer.js";
import {applicationSettings} from "../../config/app-settings.js";

const cockpit_html = {
    props: {
        data: Object
    },
    template:
        `
        <div class="cockpit_html_container" v-html="data.settings.html"></div>
        `
}

importer.appendCssFile('/themes/'+applicationSettings.pageTheme+'/cockpit-partials/html.css',function () {})

export {cockpit_html}
import * as THREE from '../../lib/threejs/build/three.module.js'
import {OBJLoader} from "../../lib/threejs/examples/jsm/loaders/OBJLoader.js";
import { OrbitControls } from 'https://cdn.skypack.dev/three/examples/jsm/controls/OrbitControls'
import { STLLoader } from 'https://cdn.skypack.dev/three/examples/jsm/loaders/STLLoader'
import Stats from 'https://cdn.skypack.dev/three/examples/jsm/libs/stats.module'

import * as importer from "../../js/importer.js";
import {applicationSettings} from "../../config/app-settings.js";

const cockpit_3d = {
    props: {
        data: Object
    },
    data: function () {
        return {
            randomModelContainerId: Math.floor(Math.random() * (99999999 - 10000000))
        }
    },
    mounted: function() {
        let vue = this;

                let file = applicationSettings.cockpitHost + '/' + vue.data.settings.file;
                console.log('3D FILE: '+file);

                const scene = new THREE.Scene()
                scene.background = new THREE.Color( 0xffffff );

                const light = new THREE.SpotLight()
                light.position.set(50, 50, 50)
                scene.add(light)

                const camera = new THREE.PerspectiveCamera(
                    75,
                    window.innerWidth / window.innerHeight,
                    0.1,
                    1000
                )
                camera.position.z = 3
                const renderer = new THREE.WebGLRenderer()
                renderer.outputEncoding = THREE.sRGBEncoding
                renderer.setSize(window.innerWidth, window.innerHeight)
                document.getElementById(vue.randomModelContainerId).appendChild(renderer.domElement)
                const controls = new OrbitControls(camera, renderer.domElement)
                controls.enableDamping = true
                const loader = new OBJLoader();
                loader.load(
                    '/media/3D/176703-000-00-_-.obj',
                    function ( object ) {
                        scene.add( object );
                    },
                    function ( xhr ) {
                        console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );
                    },
                    function ( error ) {
                        console.log( 'An error happened: ' +error );
                    }
                );

                function animate() {
                    requestAnimationFrame(animate)

                    controls.update()

                    render()

                    //stats.update()
                }

                function render() {
                    renderer.render(scene, camera)
                }

                animate()

    },
    template:
        `
        <div class="cockpit_3d_container">
            <h2>Interactive 3D Model</h2>
            <div class="model_container" :id="randomModelContainerId"></div>
        </div>
        `
}

importer.appendCssFile('/themes/'+applicationSettings.pageTheme+'/cockpit-partials/3d.css',function () {})

export {cockpit_3d}
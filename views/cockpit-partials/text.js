import * as importer from "../../js/importer.js";
import {applicationSettings} from "../../config/app-settings.js";

const cockpit_text = {
    props: {
        data: Object
    },
    template:
        `
        <div v-html="data.settings.text"></div>
        `
}

importer.appendCssFile('/themes/'+applicationSettings.pageTheme+'/cockpit-partials/text.css',function () {})

export {cockpit_text}
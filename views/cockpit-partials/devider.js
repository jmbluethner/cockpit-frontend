import * as importer from "../../js/importer.js";
import {applicationSettings} from "../../config/app-settings.js";

const cockpit_divider = {
    props: {
        data: Object
    },
    template:
        `
        <div class="cockpit_devider">
            <hr>
        </div>
        `
}

importer.appendCssFile('/themes/'+applicationSettings.pageTheme+'/cockpit-partials/devider.css',function () {})

export {cockpit_divider}
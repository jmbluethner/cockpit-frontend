import * as importer from '../../js/importer.js';
import {applicationSettings} from "../../config/app-settings.js";
import {callApi} from "../../js/api-client.js";
import {logToConsole} from "../../js/logger.js";

const cockpit_form = {
    props: {
        data: Object
    },
    data: function () {
        return {

        }
    },
    mounted: function() {
        let vue = this;
    },
    methods: {
        sendFormData: function () {
            let vue = this;
            let targetForm = vue.data.targetForm;
            let reqBody = JSON.stringify(vue.data)

            callApi('/api/forms/submit/'+targetForm,'','POST','application/json',function (response) {
                logToConsole('Got '+response.httpstatus+' while sending form to the Cockpit API.','info');
            });
        }
    },
    template:
        `
        <div class="form_container">
            <form onsubmit="return false">
                <div v-for="field in data.fields">
                    <input v-if="field.tag === 'input'" :class="field.class" :style="field.style" :type="field.type" :placeholder="field.placeholder" :required="field.required" />
                    <input v-if="field.tag === 'textarea'" :class="field.class" :style="field.style" :placeholder="field.placeholder" :required="field.required" />
                </div>
                <button @click="sendFormData" type="submit"></button>
            </form>
        </div>
    `
}

// Load the components CSS
importer.appendCssFile('/themes/'+applicationSettings.pageTheme+'/cockpit-partials/form.css',function () {})

// Export the component
export {cockpit_form}
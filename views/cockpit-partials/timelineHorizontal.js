import * as importer from "../../js/importer.js";
import {applicationSettings} from "../../config/app-settings.js";

const cockpit_timeline_horizontal = {
    props: {
        data: Object
    },
    mounted: function () {

    },
    template:
        `
        <div class="timeline_horizontal_container">
            <div v-for="entry in data.settings.data" class="entry">
                <div class="inner">
                    <div class="top">
                        <span v-if="entry.dat" class="dat"><i class="fas fa-map-pin"></i>{{entry.dat}}</span>
                        <span class="text">{{entry.text}}</span>
                    </div>
                    <div class="bottom">
                        <a v-if="entry.linkText && entry.linkContent" :href="entry.linkContent" :target="entry.linkTarget">&#187; {{entry.linkText}}</a>
                    </div>
                </div>
            </div>
        </div>
        `
}

importer.appendCssFile('/themes/'+applicationSettings.pageTheme+'/cockpit-partials/timelineHorizontal.css',function () {})

export {cockpit_timeline_horizontal}
import {cockpit_images} from "./image.js";
import {cockpit_headings} from "./heading.js";
import {cockpit_section} from "./section.js";
import {cockpit_html} from "./html.js";
import {cockpit_text} from "./text.js";
import {cockpit_divider} from "./devider.js";
import * as importer from "../../js/importer.js";
import {applicationSettings} from "../../config/app-settings.js";
import {cockpit_gallery} from "./gallery.js";
import {cockpit_form} from "./form.js";
import {cockpit_map} from "./map.js";
import {cockpit_button} from "./button.js";
import {cockpit_youtube_video} from "./youtube-video.js";
import {cockpit_banner} from "./banner.js";
import {cockpit_tile} from "./tile.js";
import {cockpit_timeline_horizontal} from "./timelineHorizontal.js";
import {cockpit_timeline_vertical} from "./timelineVertical.js";

const cockpit_grid = {
    name: "c_grid",
    props: {
        data: Object
    },
    components: {
        c_image: cockpit_images,
        c_heading: cockpit_headings,
        c_section: cockpit_section,
        c_html: cockpit_html,
        c_text: cockpit_text,
        c_divider: cockpit_divider,
        c_gallery: cockpit_gallery,
        c_form: cockpit_form,
        c_map: cockpit_map,
        c_button: cockpit_button,
        c_yt_video: cockpit_youtube_video,
        c_banner: cockpit_banner,
        c_tile: cockpit_tile,
        c_timeline_vertical: cockpit_timeline_vertical,
        c_timeline_horizontal: cockpit_timeline_horizontal
    },
    template:
        `
        <div class="row">
            <div v-for="col in data.columns" :class="col.settings.class" class="col">
                <div v-for="obj in col.children">
                    <c_image v-if="obj.component === 'image'" v-bind:data="obj"></c_image>
                    <c_heading v-if="obj.component === 'heading'" v-bind:data="obj"></c_heading>
                    <c_grid v-if="obj.component === 'grid'" v-bind:data="obj"></c_grid>
                    <c_section v-if="obj.component === 'section'" v-bind:data="obj"></c_section>
                    <c_html v-if="obj.component === 'html'" v-bind:data="obj"></c_html>   
                    <c_text v-if="obj.component === 'text'" v-bind:data="obj"></c_text>    
                    <c_divider v-if="obj.component === 'divider'" v-bind:data="obj"></c_divider>
                    <c_gallery v-if="obj.component === 'gallery'" v-bind:data="obj"></c_gallery>
                    <c_form v-if="obj.component === 'form'" v-bind:data="obj"></c_form>
                    <c_map v-if="obj.component === 'map'" v-bind:data="obj"></c_map> 
                    <c_button v-if="obj.component === 'button'" v-bind:data="obj"></c_button>
                    <c_yt_video v-if="obj.component === 'youtubevideo'" v-bind:data="obj"></c_yt_video>
                    <c_banner v-if="obj.component === 'banner'" v-bind:data="obj"></c_banner>
                    <c_tile v-if="obj.component === 'tile'" v-bind:data="obj"></c_tile>
                    <c_timeline_vertical v-if="obj.component === 'timelineVertical'" v-bind:data="obj"></c_timeline_vertical>
                    <c_timeline_horizontal v-if="obj.component === 'timelineHorizontal'" v-bind:data="obj"></c_timeline_horizontal>
                </div>
            </div>
        </div>
        `
}

importer.appendCssFile('/themes/'+applicationSettings.pageTheme+'/cockpit-partials/grid.css',function () {})

export {cockpit_grid}
import {cockpit_images} from "./image.js";
import {cockpit_headings} from "./heading.js";
import {cockpit_grid} from "./grid.js";
import {cockpit_html} from "./html.js";
import {cockpit_text} from "./text.js";
import {cockpit_divider} from "./devider.js";
import * as importer from "../../js/importer.js";
import {applicationSettings} from "../../config/app-settings.js";
import {cockpit_gallery} from "./gallery.js";
import {cockpit_map} from "./map.js";
import {cockpit_form} from "./form.js";

const cockpit_section = {
    name: "c_section",
    props: {
        data: Object
    },
    components: {
        c_image: cockpit_images,
        c_heading: cockpit_headings,
        //c_grid: cockpit_grid,
        c_html: cockpit_html,
        c_text: cockpit_text,
        c_divider: cockpit_divider,
        c_gallery: cockpit_gallery,
        c_map: cockpit_map,
        c_form: cockpit_form
    },
    template:
        `
        <section>
            <div v-for="obj in data.children" class="section_inner">
                <c_image v-if="obj.component === 'image'" v-bind:data="obj"></c_image>
                <c_heading v-if="obj.component === 'heading'" v-bind:data="obj"></c_heading>
                <!-- <c_grid v-if="obj.component === 'grid'" v-bind:data="obj"></c_grid> -->
                <c_section v-if="obj.component === 'section'" v-bind:data="obj"></c_section>
                <c_html v-if="obj.component === 'html'" v-bind:data="obj"></c_html>
                <c_text v-if="obj.component === 'text'" v-bind:data="obj"></c_text>   
                <c_devider v-if="obj.component === 'divider'" v-bind:data="obj"></c_devider>              
                <c_gallery v-if="obj.component === 'gallery'" v-bind:data="obj"></c_gallery>      
                <c_map v-if="obj.component === 'map'" v-bind:data="obj"></c_map>   
                <c_form v-if="obj.component === 'form'" v-bind:data="obj"></c_form>                        
            </div>
        </section>
        `
}

importer.appendCssFile('/themes/'+applicationSettings.pageTheme+'/cockpit-partials/section.css',function () {})

export {cockpit_section}
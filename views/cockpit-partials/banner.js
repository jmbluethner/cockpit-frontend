import * as importer from "../../js/importer.js";
import {applicationSettings} from "../../config/app-settings.js";

const cockpit_banner = {
    props: {
        data: Object
    },
    mounted: function () {
        let vue = this;
        if(vue.data.settings.picture) {
            vue.data.settings.picture = applicationSettings.cockpitHost + '/' + vue.data.settings.picture
        }
    },
    template:
        `
        <div class="banner_container" :style="'background-image: url('+data.settings.picture+')'">
            <span>{{data.settings.text}}</span>
            <a v-if="data.settings.buttonText && data.settings.buttonLink" :href="data.settings.buttonLink" :target="data.settings.buttonTarget"><button>&#187; {{data.settings.buttonText}}</button></a>
        </div>
        `
}

importer.appendCssFile('/themes/'+applicationSettings.pageTheme+'/cockpit-partials/banner.css',function () {})

export {cockpit_banner}
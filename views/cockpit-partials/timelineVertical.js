import * as importer from "../../js/importer.js";
import {applicationSettings} from "../../config/app-settings.js";

const cockpit_timeline_vertical = {
    props: {
        data: Object
    },
    mounted: function () {

    },
    template:
        `
        <div class="timeline_vertical_container">
            <div v-for="entry in data.settings.data" class="entry">
                <span v-if="entry.dat" class="heading"><i class="fas fa-map-pin"></i>{{entry.dat}}</span>
                <span class="text">{{entry.text}}</span>
                <a v-if="entry.linkText && entry.linkContent" :href="entry.linkContent" :target="entry.linkTarget">&#187; {{entry.linkText}}</a>
            </div>
        </div>
        `
}

importer.appendCssFile('/themes/'+applicationSettings.pageTheme+'/cockpit-partials/timelineVertical.css',function () {})

export {cockpit_timeline_vertical}
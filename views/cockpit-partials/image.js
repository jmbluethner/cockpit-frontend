import {applicationSettings} from "../../config/app-settings.js";
import * as importer from "../../js/importer.js";

const cockpit_images = {
    props: {
        data: Object
    },
    data: function () {
        return {
            applicationSettings: applicationSettings
        }
    },
    methods: {
        cutFileExtension: function (file) {
            // Get filename from path
            file = file.replace(/^.*[\\\/]/, '');
            // Cut extension
            file = file.replace(/\.[^/.]+$/, "");
            // Replace underscore with whitespace
            file = file.replace(/_/g, ' ');
            return file;
        }
    },
    mounted: function () {

    },
    template:
        `
        <img :src="applicationSettings.cockpitHost+''+data.settings.image.path" :alt="cutFileExtension(data.settings.image.path)" />
        `
}

importer.appendCssFile('/themes/'+applicationSettings.pageTheme+'/cockpit-partials/image.css',function () {})

export {cockpit_images}
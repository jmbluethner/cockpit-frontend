import * as importer from "../../js/importer.js";
import {applicationSettings} from "../../config/app-settings.js";

const cockpit_tile = {
    props: {
        data: Object
    },
    mounted: function () {
        let vue = this;
        if(vue.data.settings.picture) {
            vue.data.settings.picture = applicationSettings.cockpitHost + '/' + vue.data.settings.picture
        }
    },
    template:
        `
        <div class="tile_container" :style="'background-image: url('+data.settings.picture+')'">
            <div class="overlay" :style="'background-color: '+data.settings.overlayColor">
                <div class="upper">
                    <span>{{data.settings.text}}</span>
                </div>
                <div class="lower">
                    <a v-if="data.settings.buttonLink && data.settings.buttonText" :href="data.settings.buttonLink" :target="data.settings.buttonTarget"><button>{{data.settings.buttonText}}</button></a>
                </div>
            </div>
        </div>
        `
}

importer.appendCssFile('/themes/'+applicationSettings.pageTheme+'/cockpit-partials/tile.css',function () {})

export {cockpit_tile}
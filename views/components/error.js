import * as importer from '../../js/importer.js';
import {applicationSettings} from "../../config/app-settings.js";
import {readGetParameter} from "../../js/url-handler.js";

const error = {
    props: {
        // Set this to true if the navbar should make it so that the next element starts below the navbar and not behind it
        backlayNavbar: Boolean
    },
    data: function () {
        return {
            errorType: '',
            errorCode: '',
            errorMsg: ''
        }
    },
    mounted: function() {
        let vue = this;
        vue.errorType = readGetParameter('type');
        vue.errorMsg = readGetParameter('message');
        vue.errorCode = readGetParameter('httpcode');
    },
    template:
    `
        <div id="error_wrapper">
            <div id="error_inner">
                <div v-if="errorType === 'internal'">
                    <h1>Internal Error</h1>
                    <span>{{errorMsg}}</span>
                </div>
                <div v-if="errorType === 'http'">
                    <h1>{{errorCode}}</h1>
                    <span>Unfortunately there was an error while processing your request. If you think this is a technical error or bug, please contact the administrator.</span>
                </div>
            </div>
        </div>
    `
}

// Load the components CSS
importer.appendCssFile('/themes/'+applicationSettings.pageTheme+'/components/error.css',function () {})

// Export the component
export {error}
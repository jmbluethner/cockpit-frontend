import {applicationSettings} from "../../config/app-settings.js";

// Import Cockpit partials (CMS objects for the layout field type)
// @TODO import on demand for better performance and less download size
import {cockpit_headings} from "../cockpit-partials/heading.js";
import {cockpit_images} from "../cockpit-partials/image.js";
import {cockpit_grid} from "../cockpit-partials/grid.js";
import {cockpit_section} from "../cockpit-partials/section.js";
import {cockpit_html} from "../cockpit-partials/html.js";
import {cockpit_text} from "../cockpit-partials/text.js";
import {cockpit_divider} from "../cockpit-partials/devider.js";
import {cockpit_gallery} from "../cockpit-partials/gallery.js";
import {cockpit_button} from "../cockpit-partials/button.js";
import {cockpit_form} from "../cockpit-partials/form.js";
import {cockpit_map} from "../cockpit-partials/map.js";
import {cockpit_youtube_video} from "../cockpit-partials/youtube-video.js";
import {cockpit_spacer} from "../cockpit-partials/spacer.js";
import {cockpit_video} from "../cockpit-partials/video.js";
//import {cockpit_3d} from "../cockpit-partials/3d.js";
import {cockpit_banner} from "../cockpit-partials/banner.js";
import {cockpit_tile} from "../cockpit-partials/tile.js";
import {cockpit_timeline_vertical} from "../cockpit-partials/timelineVertical.js";
import {cockpit_timeline_horizontal} from "../cockpit-partials/timelineHorizontal.js";

const pagecontents = {
    props: {

    },
    data: function () {
        return {
            data: {},
            applicationSettings: applicationSettings
        }
    },
    methods: {
        // Get data from the root component. Function gets triggered by root.
        fetchData: function() {
            this.data = this.$root.pageData.pagecontent;
            this.normalizeData();
        },
        // Go through the received data and normalize/default some things
        normalizeData: function () {
            let vue = this;
            this.data.forEach(function (item,index) {
                switch(item.component) {
                    case 'heading':
                        if(item.settings.tag === null) {
                            item.settings.tag = 'h1';
                        }
                        break;
                    case 'grid':

                        break;
                }
            });
        }
    },
    mounted: function () {
        let vue = this;
    },
    components: {
        c_heading: cockpit_headings,
        c_image: cockpit_images,
        c_grid: cockpit_grid,
        c_section: cockpit_section,
        c_html: cockpit_html,
        c_text: cockpit_text,
        c_divider: cockpit_divider,
        c_gallery: cockpit_gallery,
        c_button: cockpit_button,
        c_form: cockpit_form,
        c_map: cockpit_map,
        c_yt_video: cockpit_youtube_video,
        c_spacer: cockpit_spacer,
        c_video: cockpit_video,
        c_banner: cockpit_banner,
        c_tile: cockpit_tile,
        c_timeline_vertical: cockpit_timeline_vertical,
        c_timeline_horizontal: cockpit_timeline_horizontal
        //c_3d: cockpit_3d
    },
    template:
        `
        <section class="cockpit-container">
            <div v-for="content in data" :data-cockpit-component="content.component">

                <c_heading v-if="content.component === 'heading'" v-bind:data="content"></c_heading>
                <c_image v-if="content.component === 'image'" v-bind:data="content"></c_image>      
                <c_grid v-if="content.component === 'grid'" v-bind:data="content"></c_grid>
                <c_section v-if="content.component === 'section'" v-bind:data="content"></c_section>
                <c_html v-if="content.component === 'html'" v-bind:data="content"></c_html>  
                <c_text v-if="content.component === 'text'" v-bind:data="content"></c_text>         
                <c_divider v-if="content.component === 'divider'" v-bind:data="content"></c_divider>   
                <c_gallery v-if="content.component === 'gallery'" v-bind:data="content"></c_gallery>   
                <c_button v-if="content.component === 'button'" v-bind:data="content"></c_button>
                <c_form v-if="content.component === 'form'" v-bind:data="content"></c_form>
                <c_map v-if="content.component === 'Map'" v-bind:data="content"></c_map>
                <c_yt_video v-if="content.component === 'youtubevideo'" v-bind:data="content"></c_yt_video>
                <c_spacer v-if="content.component === 'spacer'" v-bind:data="content"></c_spacer>
                <c_video v-if="content.component === 'video'" v-bind:data="content"></c_video>
                <c_banner v-if="content.component === 'banner'" v-bind:data="content"></c_banner>
                <c_tile v-if="content.component === 'tile'" v-bind:data="content"></c_tile>
                <c_timeline_vertical v-if="content.component === 'timelineVertical'" v-bind:data="content"></c_timeline_vertical>
                <c_timeline_horizontal v-if="content.component === 'timelineHorizontal'" v-bind:data="content"></c_timeline_horizontal>
                <!-- c_3d v-if="content.component === '3dmodel'" v-bind:data="content"></c_3d -->

            </div>
        </section>
        `
}

// Export the component
export {pagecontents}
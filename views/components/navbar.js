import * as importer from '../../js/importer.js';
import {applicationSettings} from "../../config/app-settings.js";
import {callApi} from "../../js/api-client.js";

const navbar = {
    props: {
        // Set this to true if the navbar should make it so that the next element starts below the navbar and not behind it
        backlayNavbar: Boolean
    },
    data: function () {
        return {
            data: {
                entries: [],
            },
            submenus: [],
            activeSubmenu: ''
        }
    },
    mounted: function () {
        let vue = this;
        callApi('/api/singletons/get/navbar','','GET','',function (response) {
            vue.data = response;
            vue.data.logo = applicationSettings.cockpitHost+'/'+response.logo;
            vue.data.entries.forEach(function(item,index) {
                if(item.submenus) {
                    vue.submenus[vue.submenus.length] = item.submenus;
                }
            });
        });
        let container = document.getElementById('navbar_container');
        document.addEventListener('scroll',function (){
            if(window.scrollY > 0 ) {
                container.classList.add('compressed');
            } else {
                container.classList.remove('compressed');
            }
        });
    },
    methods: {

    },
    template:
    `
        <div id="navbar_wrapper">
            <div v-if="backlayNavbar" class="navbar_backlay"></div>
            <div id="navbar_container">
                <div id="navbar_ribbon">
                    <div class="inner">
                        <div class="left">
                            <span v-if="data.phone"><i class="fas fa-phone"></i>{{data.phone}}</span>
                            <span v-if="data.email"><i class="fas fa-envelope"></i>{{data.email}}</span>
                        </div>
                        <div class="right">
                            <a v-for="social in data.socials" :href="social.link" target="_blank"><i :class="'fab fa-'+social.icon"></i></a>
                        </div>
                    </div>
                </div>
                <div class="inner">
                    <div class="navbar_logo_container">
                        <a href="/"><img class="navbar_logo" :src="data.logo" alt="Logo"/></a>
                    </div>
                    <div class="navbar_nav_entry_container">            
                        <span v-for="navEntry in data.entries">
                            <a v-if="!navEntry.submenus" :href="navEntry.link" :target="navEntry.target"><button class="nav_entry">{{navEntry.title}}</button></a>
                            <a v-if="navEntry.submenus"><button class="nav_entry" @mouseover="activeSubmenu = navEntry.title" @mouseleave="activeSubmenu = ''">{{navEntry.title}} <i class="fas fa-caret-down"></i></button></a>
                        </span>
                    </div>
                </div>
                
                <Transition>
                    <div v-for="navEntry in data.entries" id="navbar_megamenu_container" v-if="navEntry.submenus !== '' && activeSubmenu == navEntry.title" @mouseover="activeSubmenu = navEntry.title" @mouseleave="activeSubmenu = ''">
                        <div class="inner" :data-parent-menu="navEntry.title">
                            <div class="group" v-for="group in navEntry.submenus">
                                <h6>{{group.name}}</h6>
                                <ul>
                                    <a v-for="link in group.links" :href="navEntry.link + '' +link.link" :target="link.target"><li>{{link.title}}</li></a>
                                </ul>
                            </div>
                        </div>
                    </div>
                </Transition>
                    
            </div>
        </div>
    `
}

// Load the components CSS
importer.appendCssFile('/themes/'+applicationSettings.pageTheme+'/components/navbar.css',function () {})

// Export the component
export {navbar}
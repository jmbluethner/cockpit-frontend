import * as importer from '../../js/importer.js';
import {applicationSettings} from "../../config/app-settings.js";
import {callApi} from "../../js/api-client.js";

const customfooter = {
    props: {

    },
    data: function () {
        return {
            data: Object
        }
    },
    mounted: function() {
        let vue = this;
        callApi('/api/singletons/get/footer','','GET','',function (response) {
            vue.data = response;
        });
    },
    template:
        `
        <div id="footer_container">
            <div class="triangle"></div>
            <footer>
                <div class="row">
                    <div class="col three" v-html="data.Column_1"></div>
                    <div class="col three" v-html="data.Column_2"></div>
                    <div class="col three" v-html="data.Column_3"></div>
                    <div class="col three" v-html="data.Column_4"></div>
                </div>
                <div class="lower_container">
                    <div class="split">
                        <div class="legal">
                            <a v-for="(legal,index) in data.legal_notice" :href="legal.link">
                                <span v-if="index < data.legal_notice.length-1">{{legal.name}} <span>|</span></span>
                                <span v-if="index == data.legal_notice.length-1">{{legal.name}}</span>
                            </a>
                        </div>
                        <div class="social">
                            <a v-for="icon in data.social_links" :href="icon.link"><i :class="icon.icon"></i></a>
                        </div>
                    </div>         
                    <span class="copyright" v-html="data.copyright_notice"></span>
                </div>
            </footer>
        </div>
    `
}

// Load the components CSS
importer.appendCssFile('/themes/'+applicationSettings.pageTheme+'/components/footer.css',function () {})

// Export the component
export {customfooter}
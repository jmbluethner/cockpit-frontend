import * as importer from '../../js/importer.js';
import {applicationSettings} from "../../config/app-settings.js";
import * as cookiehandler from "../../js/cookie-handler.js"
import * as analyticsReporter from "../../js/analytics-reporter.js";

const cookiebox = {
    data: function () {
        return {

        }
    },
    methods: {
        closeCookieOverlay: function () {
            document.getElementById('cookiebox_container').style.display = 'none';
        },
        acceptAllCookies: function() {
            analyticsReporter.sendAnonymousStatistics();
            cookiehandler.acceptCookies();
            this.closeCookieOverlay();
        },
        acceptCookiesWithSettings: function () {

        }
    },
    created: function() {

    },
    mounted: function() {
        let accs = document.getElementsByClassName("accordion");
        let spans = document.querySelectorAll("button.accordion > span")
        Array.from(accs).forEach(function (acc,index) {
            spans[index].addEventListener("click", function() {
                acc.classList.toggle("active");
                let panel = acc.nextElementSibling;
                if (panel.style.maxHeight) {
                    panel.style.maxHeight = null;
                } else {
                    panel.style.maxHeight = panel.scrollHeight + "px";
                }
            });
        })
    },
    template: `
        <div id="cookiebox_container">
            <div id="cookiebox_inner_wrapper">
                <span class="heading">Datenschutz ist uns wichtig</span>
                <p>Bevor wir starten können, müssen wir dich über unseren Einsatz von Cookies und Analysedaten informieren.<br>Keine Sorge - Cookies sind nichts böses! Sie dienen uns nur dazu, dir unseren Service bereitstellen zu können. Sofern wir selbst Daten erheben, geben wir diese niemals an Dritte weiter.</p>
                
                <button class="accordion">
                    <span>Essenzielle Cookies</span>
                </button>
                <div class="panel">
                  <p>Die essenziellen Cookies sind die, die wir unbedingt setzen müssen um die Grundfunktionalität der Seite zu gewährleisten. Diese benötigen wir immer.</p>
                </div>
                
                <button class="accordion">
                    <span>Feature Cookies</span>
                    <input type="checkbox" class="toggle" checked>
                </button>
                <div class="panel">
                  <p>Die Feature Cookies sind die, die wir für Funktionen wie z.B. Videoplayer etc. verwenden. Diese sind zwar nicht absolut essenziell, aber sie können maßgeblich zum Erscheinungsbild der Seite beitragen.</p>
                </div>
                
                <button class="accordion">
                    <span>Analysedaten</span>
                    <input type="checkbox" class="toggle" checked>
                </button>
                <div class="panel">
                  <p>Die Analyse Cookies helfen uns sehr bei der Auswertung der Website-Performance. Hier weden <b>anonyme</b> Nutzerdaten an uns gesendet, die es uns jedoch zu keiner Zeit erlauben, individuelle Rückschlüsse auf Personen zu ziehen.</p>
                </div>
                
                <div class="buttons_container">
                    <button @click="acceptCookiesWithSettings">Einstellungen übernehmen</button>
                    <button @click="acceptAllCookies">Alle akzeptieren</button>
                </div>
                
            </div>
        </div>
    `
}

// Load the components CSS
importer.appendCssFile('/themes/'+applicationSettings.pageTheme+'/components/cookiebox.css',function () {})

// Export the component
export {cookiebox}
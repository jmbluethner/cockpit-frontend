// Import js
import {logToConsole} from "../../js/logger.js"
import {prepareVue} from "../main.js";
import {loadBaseCss} from "../../js/baseloader.js";
import {setPageMeta} from "../../js/metasetter.js";
import {checkApiConnection} from "../../js/api-check.js";
import * as cockpit from "../../js/cockpit.js";
import * as cookiehandler from "../../js/cookie-handler.js";
import {getCurrentPage} from "../../js/url-handler.js";

// Import components
import {navbar} from "../components/navbar.js";
import {pagecontents} from "../components/pagecontent.js";
import {customfooter} from "../components/footer.js";
import {cockpit_form} from "../cockpit-partials/form.js";

// Main Component
prepareVue(function () {

    loadBaseCss();

    logToConsole('Initializing Vue views...','info');


    let bus = {};
    bus.eventBus = new Vue();

    // Create app
    let app = new Vue({
        el: '#app',
        components: {
            navbar: navbar,
            pagecontent: pagecontents,
            customfooter: customfooter
        },
        data: function () {
            return {
                pageData: {}
            }
        },
        mounted: function() {
            let vue = this;

            let currPage = getCurrentPage();

            cockpit.getPageData(currPage,function (response) {
                vue.pageData = response;
                setPageMeta(vue.pageData);
                vue.$refs.contentcontainer.fetchData();
            });

        },
        template:
            `
            <div>
                <navbar v-bind:backlayNavbar="true" />
                <pagecontent ref="contentcontainer"></pagecontent>
                <customfooter></customfooter>
            </div>
            `
    });
})
// Import js
import {logToConsole} from "../../js/logger.js"
import {prepareVue} from "../main.js";
import {loadBaseCss} from "../../js/baseloader.js";
import {setPageMeta} from "../../js/metasetter.js";

import * as cookiehandler from "../../js/cookie-handler.js";

// Import components
import {error} from "../components/error.js";

// Main Component
prepareVue(function () {

    loadBaseCss();
    setPageMeta();

    logToConsole('Initializing Vue views...','info');

    let bus = {};
    bus.eventBus = new Vue();

    // Create app
    let app = new Vue({
        // Use div#app as container
        el: '#app',
        // define all components
        components: {
            error: error
        },
        // build the template
        template:
            `
            <div>
                <error></error>
            </div>
            `
    });
})
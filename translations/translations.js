export const availbaleLanguages =
    {
        de: {
            language: "Deutsch",
            country: "",
            flag: "de"
        },
        fr: {
            language: "Français",
            country: "France",
            flag: "fr"
        },
        en: {
            language: "Englisch",
            country: "America / England",
            flag: "gb"
        }
    };

export const defaultLanguage = 'de';

export const translations =
    {
        "fieldPageHeadline": {
            de: "Willkommen",
            en: "Welcome"
        }
    };